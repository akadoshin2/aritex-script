import Consol from '../../console';
import {parseString} from 'xml2js';
import request from 'request';
const console = new Consol();

console.registerApp("CreateRequest", 'Register class CreateRequest');
export default class createRequest{
    constructor(props) {
        this.GetDataUrl = 'http://190.85.249.115/WSUNOEE/WSUNOEE.asmx';
        this.headers = {
            'Content-Type': 'text/xml',
            'cache-control': 'no-cache',
        }
        this.options = {
            url: this.GetDataUrl,
            method: 'POST',
            qs: { WSDL: '' },
            headers: this.headers,
            body: ""
        }
    }
    on(body, callback){
        console.yellow(`Request loading...`);
        this.options.body = `
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
        <soapenv:Header/>
        <soapenv:Body>
            <tem:EjecutarConsultaXML>
                <!--Optional:-->
                <tem:pvstrxmlParametros>
                <![CDATA[
                    <Consulta>
                        <NombreConexion>real</NombreConexion>
                        <IdCia>1</IdCia>
                        <IdProveedor>IS</IdProveedor>
                        <IdConsulta>${body}</IdConsulta>
                        <Usuario>usuariogt</Usuario>
                        <Clave>gt2017</Clave>
                        <Parametros></Parametros>
                    </Consulta> 
                ]]>
                </tem:pvstrxmlParametros>
            </tem:EjecutarConsultaXML>
        </soapenv:Body>
        </soapenv:Envelope>`;
        //console.cyan(this.options);
        request(this.options, function (error, response, body) {
            if (error){
                console.log(error, response, body) 
                throw new Error(error);
            }
            parseString(body, function (err, result) {
                try {
                    let _result = result['soap:Envelope']['soap:Body'][0]
                                        ['EjecutarConsultaXMLResponse'][0]
                                        ['EjecutarConsultaXMLResult'][0]
                                        ['diffgr:diffgram'][0]
                                        ['NewDataSet'][0]
                                        ['Resultado'];
                    callback(error, response, _result);
                } catch (error) {
                    let _error =  result['soap:Envelope']['soap:Body'][0]
                                        ['soap:Fault'][0]
                                        ['faultstring'][0];
                    console.red(_error);
                }       
            });
            console.cyan("Request finish");
        });          
    }
    
}
