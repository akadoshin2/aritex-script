import CreateRequest from './createRequest';
import Consol from '../console';
const console = new Consol();

console.registerApp("SoapResquest", 'Register class SoapResquest');
export default class SoapResquest extends CreateRequest{
    constructor(props){
        super(props);
        this.Promises = {
            PromiseClientes: null,
            PromiseInventario: null,
            PromiseItems: null,
            PromisePrecios: null
        }
    }
    _PromiseRequest(name){
        console.yellow(`Request from ${name}...`)
        return new Promise((resolve, reject)=>{
            this.on(name, (error, response, body)=>{
                resolve({error, response, body, name});
            })
        });
    }
    getClientes(callback){
        this.Promises.PromiseClientes = this._PromiseRequest('clientes').then(({response, body, name})=>{
            console.cyan(`Request from ${name} finish, width ${body.length} items.`);
            callback(body, response, name);
        });
        
    }
    getInventario(callback){
        this.Promises.PromiseInventario = this._PromiseRequest('inventario').then(({response, body, name})=>{
            console.cyan(`Request from ${name} finish, width ${body.length} items.`);
            callback(body, response, name);
        });
    }
    getItems(callback){
        this.Promises.PromiseItems = this._PromiseRequest('items').then(({response, body, name})=>{
            console.cyan(`Request from ${name} finish, width ${body.length} items.`);
            callback(body, response, name);
        });
    }
    getPrecios(callback){
        this.Promises.PromisePrecios = this._PromiseRequest('precios').then(({response, body, name})=>{
            console.cyan(`Request from ${name} finish, width ${body.length} items.`);
            callback(body, response, name);
        });
    }
    allDone(ignore = false){
        let allP = [];
        for(let Ps in this.Promises){
            allP.push(this.Promises[Ps]);
            if(!ignore&&this.Promises[Ps] === null){
                console.red(`The ${Ps} promise is null`);
                return Promise.reject(new Error(`The ${Ps} promise is null`));
            }
        }
        return Promise.all(allP).then(values=>{
            console.cyan(`All the promises ended`);
            return values;
        })
    }
}