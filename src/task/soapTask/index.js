import Consol from '../../console';
import task from '../createTask';
import FirebaseTask from '../firebaseTask';

const console = new Consol();
var firebaseT = new FirebaseTask();

console.registerApp("SoapTask");
const actionUpdate = ()=>{
    // firebaseT.updateItems(()=>{
    //     firebaseT.updateInventario(()=>{
    //         firebaseT.updateClientes(()=>{
    //             console.red(firebaseT);

    //         });
    //     });
    // })
    firebaseT.updatePrecios((precios)=>{
        firebaseT.updateItems(precios, (itemsData)=>{
            console.red("itemsData finish update");
        });
    });
    firebaseT.updateClientes(()=>{
        console.red("Clientes update finish");
    });
}
firebaseT.listenOrders();
actionUpdate();

export const updateDatabase = new task({
    time: '00 00 11,21 * * 0-6',
    actionUpdate
});