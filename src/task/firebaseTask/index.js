import firebase from 'firebase-admin';
import fs from 'fs';
import request from 'request';
const serviceAccount = require("./serviceAccountKey.json");
import WooCommerceAPI from 'woocommerce-api';
import fetch from 'node-fetch';

import AWS from 'aws-sdk';
AWS.config.update({ accessKeyId: 'AKIAIHN7ALEDRLEWSNFA', secretAccessKey: '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' });


import Consol from '../../console';
const console = new Consol();

import SoapRequest from '../../soap';

const WooCommerce = new WooCommerceAPI({
    url: 'https://aritex.com.co',
    consumerKey: 'ck_66655dac6a85f4ebb02af6ab993b524f43cd3bc2',
    consumerSecret: 'cs_20354a46d591550044b87fd0a5aa21ee6256ccf9',
    wpAPI: true, 
    wpAPIPrefix: "",
    version: 'wc/v1' 
  });

Array.prototype.unique=function(a){
    return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
});

console.registerApp("FirebaseTask", 'Register class FirebaseTask');
export default class FirebaseTask extends SoapRequest{
    constructor(props) {
        super(props);
        this.firebaseData = {
            credential: firebase.credential.cert(serviceAccount),
            databaseURL: "https://reacttestaritex.firebaseio.com"
        };
        firebase.initializeApp(this.firebaseData);
        const db = firebase.database();
        this.dbRef = db.ref();
    }
    uploadJsonFirebaseRest(path, data){
        return new Promise((resolve, reject)=>{
            let options = {
                url: `${this.firebaseData.databaseURL}/ThemeApp/data/${path}.json`,
                method: 'PUT',
                body: JSON.stringify(data, null, 2)
            }
            console.yellow(`Upload data to firebase in path /ThemeApp/data/${path}...`);
            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                console.cyan(`Upload complete to firebase, path /ThemeApp/data/${path}.`);
                resolve();
            });  
        })
    }
    async getImgs(callback){
        var s3bucket = new AWS.S3();
        var params = {
            Bucket: 'aritex-s3',
            Prefix: 'products',
            Delimiter: 'products'
           }
           
        var imgs = null;
        await s3bucket.listObjects(params, (err, data) => {
            if (err) {
                console.log(err);
            } else {
                fs.writeFileSync('data/imgs.json', JSON.stringify({["name"]: data}, null, 2)); 
                callback(data.Contents);
                // $scope.allImageData = data.Contents;
            }
        });
    }
    updateClientes(callback = null){
        console.magenta("UpdateClientes in progress...");
        console.yellow('Updating clientes list...');
        this.getClientes((body, res, name)=>{
            let clientsList = [];
            let clientIdSucuId = [];
            body.forEach(cliente => {
                // console.log(cliente);
                //Model cliente
                let addClient = true;
                clientIdSucuId.forEach(idS => {
                    if(idS == `${cliente.identificacion[0].trim()}`){
                        addClient = false;
                        clientsList.forEach(client => {
                            if(idS == client.cc){
                                let addSucursal = true;
                                client.id_sucursal.forEach(sucursal => {
                                    if(sucursal == cliente.id_sucursal[0].trim()){
                                        addSucursal = false;
                                        return 0;
                                    }
                                });
                                if(addSucursal){
                                    client.id_sucursal.push(cliente.id_sucursal[0].trim());
                                    if(cliente.nombre_sucursal){
                                        client.nombre_sucursal.push(cliente.nombre_sucursal[0].trim());
                                    }else{
                                        client.nombre_sucursal.push('');
                                    }
                                    if(cliente.ciudad){
                                        client.address_sucursal.push(cliente.ciudad[0].trim());
                                    }else{
                                        client.address_sucursal.push('');
                                    }
                                    if(cliente.address_sucursal){
                                        client.city_sucursal.push(cliente.address_sucursal[0].trim());
                                    }else{
                                        client.city_sucursal.push('');
                                    }
                                }
                            }
                        });
                    }
                });
                if(addClient){
                    clientIdSucuId.push(`${cliente.identificacion[0].trim()}`);   
                    // console.log(cliente);
                    clientsList.push({
                        client_id: cliente.identificacion[0].trim(),
                        rowOrder: cliente['$']['msdata:rowOrder'].trim(),
                        cc: cliente.identificacion[0].trim(),
                        name: cliente.razon_social[0].trim(),
                        nombres: cliente.nombres[0].trim(),
                        apellido1: cliente.apellido1[0].trim(),
                        apellido2: cliente.apellido2[0].trim(),
                        id_sucursal: new Array(cliente.id_sucursal[0].trim()),
                        nombre_sucursal: new Array(cliente.nombre_sucursal?cliente.nombre_sucursal[0].trim():''),
                        address_sucursal: new Array(cliente.direccion?cliente.direccion[0].trim():''),
                        city_sucursal: new Array(cliente.ciudad?cliente.ciudad[0].trim():''),

                        email: "email",
                        user_type: "user_type",
                        dv: "dv",
                        act_code: "act_code",
                        seller_code: "seller_code",
                        status: "status",
                        client_status: "client_status",
                        attachments: [],
                        time: "time",
                        flete: "flete",
                        assigned: "assigned",
                        treasure_dept: "treasure_dept",
                        mobile: "mobile",
                        phone: "phone",
                        city: "city",
                        address: "address"
                    });
                }
            });
            let data = JSON.stringify({[name]: clientsList}, null, 2);  
            fs.writeFileSync('data/clientsList.json', data);  
            console.cyan(`${name} list update, width ${clientsList.length} items.`);
            if(clientsList.length>0){
                this.uploadJsonFirebaseRest(name, clientsList).then(()=>{
                    if(callback!=null) callback(clientsList);
                });
            }
        });
    }
    updateInventario(callback = null){
        console.magenta("UpdateInventario in progress...");
        console.yellow('Updating inventario list...');
        this.getInventario((body, res, name)=>{
            let inventarioList = {};
            body.forEach(indentario => {
                //Model inventario
                if(inventarioList[indentario.id_item[0].trim()] == undefined){
                    inventarioList[indentario.id_item[0].trim()] = [];
                }
                inventarioList[indentario.id_item[0].trim()].push({
                    id: indentario['$']['diffgr:id'].trim(),
                    rowOrder: indentario['$']['msdata:rowOrder'].trim(),
                    co: indentario.co[0].trim(),
                    bodega: indentario.bodega[0].trim(),
                    id_item: indentario.id_item[0].trim(),
                    referencia: indentario.referencia[0].trim(),
                    descripcion: indentario.descripcion[0].trim(),
                    cantidad: indentario.CANTIDAD[0].trim(),
                    cant_comprometida: indentario.CANT_COMPROMETIDA[0].trim(),
                    cant_disponible: indentario.CANT_DISPONIBLE[0].trim()
                });
            });
            let data = JSON.stringify({[name]: inventarioList}, null, 2);  
            fs.writeFileSync('data/inventarioList.json', data);  
            console.cyan(`${name} list update, width ${inventarioList.length} items.`);
            if(callback!=null) callback(data);
            // if(inventarioList.length>0){
                // this.uploadJsonFirebaseRest(name, inventarioList).then(()=>{
                //     if(callback!=null) callback(data);
                // });
            // }
        });
    }
    searchWord(inW, word){
        let e;
        for(let a in inW.items){
            (inW.items[a]).forEach(element => {
                if(element.referencia_item == word){
                    e = element.id_item;
                    return element.id_item;
                }
            });
        }
        // console.log(e);
        return e;
    }
    listenOrders(){
        const db = firebase.database();
        const orderRef = db.ref("ThemeApp/data/orders");
        orderRef.on("value", snapshot => {
            const val = snapshot.val();
            for(let valKey in val){
                let keyUnitPedido = valKey;
                let valOrder = val[valKey];
                if(valOrder.status == 2){
                    console.log(keyUnitPedido);
                    let date = new Date();
                    let dateEntrega = new Date(valOrder.date_entrega);
                    let date_ = `${date.getFullYear()}${("0"+(date.getMonth()+1)).substr(-2)}${("0"+date.getDate()).substr(-2)}`;
                    let date_entrega = `${dateEntrega.getFullYear()}${("0"+(dateEntrega.getMonth()+1)).substr(-2)}${("0"+dateEntrega.getDate()).substr(-2)}`;
                    // eslint-disable-next-line radix
                    let uiid = parseInt((new Date().getTime())/100);
                    let bodyXml = '';
                    let archivosP = fs.readFile('data/itemsList1.json', 'utf8', (err, content) => {
                        bodyXml = `
                            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gen="http://generictransfer.com/">
                            <soapenv:Header/>
                            <soapenv:Body>
                            <gen:ImportarDatosXML>
                                <gen:idDocumento>55766</gen:idDocumento>
                                <!--Optional:-->
                                <gen:strNombreDocumento>Pedido</gen:strNombreDocumento>
                                <gen:idCompania>2</gen:idCompania>
                                <!--Optional:-->
                                <gen:strCompania>1</gen:strCompania>
                                <!--Optional:-->
                                <gen:strUsuario>gt</gen:strUsuario>
                                <!--Optional:-->
                                <gen:strClave>gt</gen:strClave>
                                <!--Optional:-->
                                <gen:strFuenteDatos><![CDATA[
                                <MyDataset>
                                <Pedidos>
                                    <CentroOperacioDoctoc>001</CentroOperacioDoctoc>
                                    <NumDocto>1</NumDocto>
                                    <FechaDocto>${date_}</FechaDocto>
                                    <IndBackordeDocto>${valOrder.indBackordeDocto||2}</IndBackordeDocto>
                                    <TercerFact>${valOrder.client_id}</TercerFact>
                                    <SucursalFact>${valOrder.client.id_sucursal}</SucursalFact>
                                    <TerceroDesp>${valOrder.client_id}</TerceroDesp>
                                    <SucursalDesp>${valOrder.client.id_sucursal}</SucursalDesp>
                                    <CentroOperFac>001</CentroOperFac>
                                    <FechaEntrega>${valOrder.date_entrega}</FechaEntrega>
                                    <NumDiasEntrega>${valOrder.num_entrega}</NumDiasEntrega>
                                    <OrdenCompra>OC${uiid}</OrdenCompra>
                                    <CondPago>60D</CondPago>
                                    <Observaciones>${valOrder.observation}</Observaciones>
                                    <TerceroVend>${valOrder.user}</TerceroVend>
                                </Pedidos>
                                ${valOrder.products.map((product) => 
                                    product.variations.map((variation) =>  `
                                    <MovtoPedidos>
                                        <UnidNegocMvto>99</UnidNegocMvto>
                                        <CentroOper>001</CentroOper>
                                        <ConsecDocto>1</ConsecDocto>
                                        <NumReg>1</NumReg>
                                        <Item>${this.searchWord(JSON.parse(content), `${product.product.reference_id}-${variation.color}-${variation.size}`)}</Item>
                                        <Bodega>00101</Bodega>
                                        <Motivo>01</Motivo>
                                        <CentroOperMvto>001</CentroOperMvto>
                                        <FechaEntrga>${valOrder.date_entrega}</FechaEntrga>
                                        <NumDiasEntrega>${valOrder.num_entrega}</NumDiasEntrega>
                                        <Cantidad>${variation.quantity}</Cantidad>
                                        <IndBackorder>5</IndBackorder>
                                    </MovtoPedidos>`))}
                        </MyDataset>]]>
                                </gen:strFuenteDatos>
                                <!--Optional:-->
                                <gen:Path>C:\\inetpub\\wwwroot\\GTIntegration\\Planos</gen:Path>
                            </gen:ImportarDatosXML>
                            </soapenv:Body>
                        </soapenv:Envelope>    
                        `.replace(/,/g, '');
                        fetch('http://190.85.249.115/GTIntegration/ServiciosWeb/wsGenerarPlano.asmx', {
                            method: 'POST',
                            headers: {
                                "content-type": "text/xml",
                                "soapaction": "http://generictransfer.com/ImportarDatosXML",
                                "host": "190.85.249.115",
                                "cache-control": "no-cache"
                            },
                            body: bodyXml
                        })
                        .then(response=>response.text())
                        .then(res => {
                            console.log(res);
                            if(res.search('<ImportarDatosXMLResult>Importacion exitosa</ImportarDatosXMLResult>')){
                                orderRef.child(keyUnitPedido).child('status').set('1');
                            }else{
                                orderRef.child(keyUnitPedido).child('status').set('3');
                            }
                            fs.writeFileSync('data/resultFetch.json', res);  
                            fs.writeFileSync('data/resulXml.json', bodyXml);  
                            fs.writeFileSync('data/resultVal.json', JSON.stringify(valOrder, null, 2));  
                        });
                    });
                }
            }
        }, error => {
            console.log("The read Failed Orders: " + error.code)
        });
    }
    filterCategory(dummyData, imagenes, callback){
        let categories = {};
        let i = 0;
        
        const db = firebase.database();
        const orderRef = db.ref("ThemeApp/data/products");
        return orderRef.once("value", snapshot => {
            const productsFirebase = snapshot.val();
            // console.log(productsFirebase);

            for(let a in dummyData){
                dummyData[a].forEach(element => {
                    if(categories[element.CATEGORIA] == undefined){
                        if(element.CATEGORIA != undefined){
                            let show = true, home = false;
                            if(productsFirebase !== null){
                                let _prod_index = productsFirebase.findIndex(_prod_ => 
                                    (_prod_.id) === (element.CATEGORIA)
                                );
                                if(_prod_index !== -1){
                                    home = productsFirebase[_prod_index].home;
                                    show = productsFirebase[_prod_index].shown;
                                }
                            }
                            categories[element.CATEGORIA] = {
                                "id": element.CATEGORIA,
                                "updatedDate": new Date(),
                                "createdDate": new Date(),
                                "name": `${element.DEPARTAMENTO} ${(element.DEPARTAMENTO.indexOf(element.CATEGORIA)>=0)?'':element.CATEGORIA}`,
                                "shown": show,
                                "home": home,
                                "order_id": i+2,
                                "status": 1,
                                "slug": `${(element.DEPARTAMENTO).toLowerCase()} ${(element.CATEGORIA).toLowerCase()}`,
                                "display": "default",
                                "menu_order": 0,
                                "count": 0,
                                "products": {}
                            };
                        }
                    }
                });
                i++;
            }
            
            const db = firebase.database();
            const orderRef = db.ref("ThemeApp/data/colores");
            orderRef.once("value", snapshot => {
                const colorsFirebase = snapshot.val();

                for(let a in dummyData){
                    dummyData[a].forEach(element => {
                        if(categories[element.CATEGORIA] != undefined){
                            if(categories[element.CATEGORIA].products[element.REFERENCIA] == null){
                                categories[element.CATEGORIA].products[element.REFERENCIA] = {
                                    "updatedDate": new Date(),
                                    "createdDate": new Date(),
                                    "reference_id": element.REFERENCIA,
                                    "price": element.PRICE,
                                    "name": element.DESCRIPTION,
                                    "categories": [
                                        element.CATEGORIA
                                    ],
                                    "total_sales": 0,
                                    "purchasable": true,
                                    "on_sale": false,
                                    "sale_price": "",
                                    "regular_price": element.PRICE,
                                    "description": `${element.TELA}`,
                                    "catalog_visibility": "visible",
                                    "status": "publish",
                                    "type": "variable",
                                    "reference": element.REFERENCIA,
                                    "images": [],
                                    "attributes": [],
                                    "variations": {
                                        "color": [],
                                        "size": [],
                                        "colorId": [],
                                        "colorHex": []
                                    },
                                    "rating_count": 0,
                                    "default_attributes": []
                                };
                                imagenes.forEach(img => {
                                    let imgValid = (img.Key).replace('products/', '');
                                    if(imgValid.split("-",  1)[0] == element.REFERENCIA||imgValid.split("_",  1)[0] == element.REFERENCIA){
                                        //console.log(img)
                                        //console.log(`https://aritex-s3.s3-sa-east-1.amazonaws.com/${(img.Key).replace('products/', '')}`)
                                        categories[element.CATEGORIA].products[element.REFERENCIA].images.push({
                                            "_id": element.ID,
                                            "updatedDate": new Date(),
                                            "createdDate": new Date(),
                                            "product_id": img.Key,
                                            "src": `https://aritex-s3.s3-sa-east-1.amazonaws.com/${img.Key}`,
                                        });
                                    }
                                });
                                if(categories[element.CATEGORIA].products[element.REFERENCIA].images.length)
                                    categories[element.CATEGORIA].products[element.REFERENCIA].images.reverse();
                            }
                                if(element.COLOR !== undefined && categories[element.CATEGORIA].products[element.REFERENCIA].variations.color.indexOf(element.COLOR)<0){
                                    
                                    // if(_color_index === -1){
                                    // }
				                    let _color_index = colorsFirebase.findIndex(_color_ => 
                                        ((element.COLOR).replace(' ', '').toLowerCase()) === ((_color_.color).replace(' ', '').toLowerCase())
                                    );
                                    // console.log(`color: ${element.COLOR}, index: ${_color_index}`)
                                    categories[element.CATEGORIA].products[element.REFERENCIA].variations.color.push(element.COLOR);
                                    categories[element.CATEGORIA].products[element.REFERENCIA].variations.colorId.push((_color_index !== -1)?colorsFirebase[_color_index].colorId:'000');
                                    categories[element.CATEGORIA].products[element.REFERENCIA].variations.colorHex.push((_color_index !== -1)?colorsFirebase[_color_index].hex:'#FFF');
                                }
                                if(categories[element.CATEGORIA].products[element.REFERENCIA].variations.size.indexOf(element.TALLA)<0)
                                    categories[element.CATEGORIA].products[element.REFERENCIA].variations.size.push(element.TALLA);   
                                    
                                let sizes = categories[element.CATEGORIA].products[element.REFERENCIA].variations.size;
                                categories[element.CATEGORIA].products[element.REFERENCIA].variations.size.sort((a, b) => {
                                    return ('' + a).localeCompare(b);
                                });
                                categories[element.CATEGORIA].products[element.REFERENCIA].variations.size.reverse();
                                if(!(parseInt(sizes[0])>=0)){
                                    let sizes2 = [];
                                    sizes.forEach(size => {
                                        if(size === 'S'||size === 's') sizes2[0] = size;
                                        else if(size === 'M'||size === 'm') sizes2[1] = size;
                                        else if(size === 'L'||size === 'l') sizes2[2] = size;
                                        else if(size === 'XL'||size === 'xl') sizes2[3] = size;
                                        else if(size === 'XXL'||size === 'xxl') sizes2[4] = size;
                                    });
                                        categories[element.CATEGORIA].products[element.REFERENCIA].variations.size =  sizes2.filter(s => 1);
                                    }else{
                                        let sizes2 = [];
                                        sizes.forEach(size => {
                                            if(size == '4') sizes2[0] = size;
                                            else if(size == '8') sizes2[1] = size;
                                            else if(size == '12') sizes2[2] = size;
                                            else if(size == '16') sizes2[3] = size;
                                            else if(size == '18') sizes2[4] = size;
                                        });
                                            categories[element.CATEGORIA].products[element.REFERENCIA].variations.size =  sizes2.filter(s => 1);
                                    }
                        }
                    });
                    dummyData[a].forEach(element => {
                        if(categories[element.CATEGORIA].products[element.REFERENCIA].images.length<=0){
                            categories[element.CATEGORIA].products[element.REFERENCIA].images.push(
                                {
                                    "_id": "0001",
                                    "updatedDate": new Date(),
                                    "createdDate": new Date(),
                                    "product_id": "",
                                    "src": `https://aritex-s3.s3-sa-east-1.amazonaws.com/`,
                                });
                        }
                        if(typeof categories[element.CATEGORIA] === 'object'){
                            categories[element.CATEGORIA].count = categories[element.CATEGORIA].products.length;
                        }
                    });
                }
                console.log("finished", categories);
                callback(categories);
            });
        });
    }
    finishFilter(data){
        let products = [];
        for(let p in data){
            let _product = [];
            for(let _p in data[p].products){
                _product.push(data[p].products[_p]);
            }
            data[p].products = _product;
            _product = [];
            products.push(data[p]);
        }
        return products;
    }
    updateItems(prices, callback = null){
        console.magenta("UpdateItems in progress...");
        console.yellow('Updating items list...');
        this.getItems((body, res, name)=>{
            let itemsList = {};
            fs.writeFileSync('data/itemsListbody.json', JSON.stringify({[name]: body}, null, 2));  
            body.forEach(items => {
                //Model Items
                if(itemsList[items.id_item[0].trim()] == undefined){
                    itemsList[items.id_item[0].trim()] = [];
                }
                itemsList[items.id_item[0].trim()].push({
                    id: items['$']['diffgr:id'].trim(),
                    rowOrder: items['$']['msdata:rowOrder'].trim(),
                    id_item: items.id_item[0].trim(),
                    referencia_item: items.referencia_item[0].trim(),
                    descripcion: items.descripcion[0].trim(),
                    // barras: items.barras[0].trim(),
                    planes: items.planes[0].trim(),
                    criterio: items.criterio[0].trim(),
                    descripcion_criterio: items.descripcion_criterio[0].trim(),
                    descripcion_plan: items.descripcion_plan[0].trim()
                });
            });

            let table = {};
            for(let idElement in itemsList){
                let column = {};
                itemsList[idElement].forEach(element => {
                    if(column.ID == undefined){
                        column.ID = element.id_item;
                        column.DESCRIPTION = element.descripcion;
                        column.REFERENCIA_REQUEST = (element.referencia_item).replace(' ', '_');
                        column.BARRAS = element.barras;
                    }
                    column[element.descripcion_plan] = element.descripcion_criterio;
                    if(typeof prices[(element.referencia_item).replace(' ', '_')] === 'object'){
                        column.PRICE = parseInt((prices[(element.referencia_item).replace(' ', '_')].precio).replace('.0000', ''));
                    }
                });
                if(column!=null){
                    if(table[`${column.REFERENCIA}`] == undefined){
                        table[`${column.REFERENCIA}`] = [];
                    }
                    table[`${column.REFERENCIA}`].push(column);
                }
            }
           


            fs.writeFileSync('data/itemsList1.json', JSON.stringify({[name]: itemsList}, null, 2));  
            
            this.getImgs(imgs => {
                this.filterCategory(table, imgs, PROMISE_cat => {
                    let cat_ = PROMISE_cat;
                    fs.writeFileSync('data/itemsList1_1.json', JSON.stringify({[name]: table}, null, 2)); 
                    fs.writeFileSync('data/itemsList2.json', JSON.stringify({[name]: cat_}, null, 2));  
                    let table2 = this.finishFilter(cat_);
                    let data = JSON.stringify({[name]: table2}, null, 2);  
                    
                    setTimeout(()=>{
                        table2.forEach(itemFor => {
                            itemFor.products.forEach(productsFor => {
                                const dataSend = {
                                    name: productsFor.name,
                                    regular_price: `${productsFor.regular_price}`,
                                    sale_price: `${productsFor.price}`,
                                    type: 'simple',
                                    status: "private",
                                    description: productsFor.description,
                                    short_description: productsFor.description,
                                    catalog_visibility: productsFor.catalog_visibility,  
                                    sku: `${productsFor.reference}`,
                                    images: [],
                                    attributes: [{
                                        name: 'Talla',
                                        position: 0,
                                        visible: true,
                                        variation: true,
                                        options: []
                                      },
                                      {
                                        name: 'Color',
                                        position: 0,
                                        visible: true,
                                        variation: true,
                                        options: []
                                      }]
                                  };
                                  productsFor.images.forEach(imageProduct => {
                                      dataSend.images.push({
                                          src: imageProduct.src
                                      });
                                  });
                                  productsFor.variations.color.forEach(colorProduct => {
                                      dataSend.attributes[0].options.push(colorProduct);
                                  });
                                  productsFor.variations.size.forEach(sizeProduct => {
                                      dataSend.attributes[1].options.push(sizeProduct);
                                  });
                                  let sendRequest = new Promise((resolver, reject)=> {WooCommerce.post('products', dataSend, (err, data, res)=>{
                                        if(err){
                                            reject();
                                        }  
                                      resolver(data, res);
                                  })});
                                  
                                  sendRequest.then((data, res) => {
                                    //   console.log(res);
                                  });
                            });
                        });
                    }, 30000);
                    
                    fs.writeFileSync('data/itemsList3.json', data);  
                    console.cyan(`${name} list update, width ${data.length} items.`);
                    if(data.length>0){
                        this.uploadJsonFirebaseRest("products", table2).then(()=>{
                            if(callback!=null) callback(table2);
                        });
                    }
                });
            });
        });
    }
    joinPrices(prices){
        let finishPrice = {};
        prices.forEach(price => {
            finishPrice[(price.referencia).replace(' ', '_')] = {
                precio: price.precio,
                fecha: price.fecha_act
            }
        });
        return finishPrice;
    }
    updatePrecios(callback = null){
        console.yellow('Updating precios list...');
        this.getPrecios((body, res, name)=>{
            let preciosList = [];
            fs.writeFileSync('data/preciosListbody.json', JSON.stringify({[name]: body}, null, 2));  
            body.forEach(precio => {
                //Model Precios
                preciosList.push({
                    id: precio['$']['diffgr:id'].trim(),
                    rowOrder: precio['$']['msdata:rowOrder'].trim(),
                    id_lista: precio.id_lista[0].trim(),
                    id_item: precio.id_item[0].trim(),
                    referencia: precio.referencia[0].trim(),
                    descripcion: precio.descripcion[0].trim(),
                    precio: precio.precio[0].trim(),
                    fecha_act: precio.fecha_act[0].trim(),
                    fecha_inact: precio.fecha_inact[0].trim()
                });
            });
            let joinP = this.joinPrices(preciosList);
            let data = JSON.stringify({[name]: preciosList}, null, 2);  
            fs.writeFileSync('data/preciosList.json', data);  
            fs.writeFileSync('data/preciosList1.json', JSON.stringify({[name]: joinP}, null, 2));  
            console.cyan(`${name} list update, width ${preciosList.length} items.`);
            if(preciosList.length>0){
                if(callback!=null) callback(joinP);
                // this.uploadJsonFirebaseRest(name, preciosList).then(()=>{
                //     if(callback!=null) callback();
                // });
            }
        });
    }
    allRequestDone(){
        return this.allDone(true);
    }
    
}


