import { CronJob, CronTime } from 'cron';

// Seconds: 0-59
// Minutes: 0-59
// Hours: 0-23
// Day of Month: 1-31
// Months: 0-11 (Jan-Dec)
// Day of Week: 0-6 (Sun-Sat)

export default class CreateTast{
    constructor(props){
        let time = (props.time)? (props.time):'* * * * * *';
        this.action = (props.action)? (props.action):null;
        this.task = new CronJob({
            cronTime: time,
            onTick: this.action,
            start: false,
            timeZone: 'America/Los_Angeles'
        });
    }
    
    registerTime(time){
        this.task.setTime(CronTime(time));
    }

    run(){
        this.task.start();
    }

    isRunning(){
        return this.task.running;
    }

    stop(){
        this.task.stop();
    }
}