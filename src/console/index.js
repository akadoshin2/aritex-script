class Consol {
    constructor(){
        this.appRegister = "";
        this.colors = {
            Black: "\x1b[30m",
            Red: "\x1b[31m",
            Green: "\x1b[32m",
            Yellow: "\x1b[33m",
            Blue: "\x1b[34m",
            Magenta: "\x1b[35m",
            Cyan: "\x1b[36m",
            White: "\x1b[37m",
        }
        this.backgrounds = {
            Black: "\x1b[40m",
            Red: "\x1b[41m",
            Green: "\x1b[42m",
            Yellow: "\x1b[43m",
            Blue: "\x1b[44m",
            Magenta: "\x1b[45m",
            Cyan: "\x1b[46m",
            White: "\x1b[47m",
        }
        this.methods = {
            Reset: "\x1b[0m",
            Bright: "\x1b[1m",
            Dim: "\x1b[2m",
            Underscore: "\x1b[4m",
            Blink: "\x1b[5m",
            Reverse: "\x1b[7m",
            Hidden: "\x1b[8m",
        }
    }
    print(text, color, background, method = "Reset"){
        if(typeof text == 'object'){
            console.log(text, { depth: null });
            return 0;
        }
        console.log(`${this.colors[color]}${this.backgrounds[background]}%s${this.methods[method]}`, `${this.appRegister}${text}`);
    }
    log(text){
        console.log(text);
    }
    red(text, background = 'Black'){
        this.print(text, "Red", background);
    }
    yellow(text, background = 'Black'){
        this.print(text, "Yellow", background);
    }
    green(text, background = 'Black'){
        this.print(text, "Green", background);
    }
    cyan(text, background = 'Black'){
        this.print(text, "Cyan", background);
    }
    magenta(text, background = 'Black'){
        this.print(text, "Magenta", background);
    }
    registerApp(name, title = name){
        this.green(` - ${title}`);
        this.appRegister = `[${name}] `;
    }
}

export default Consol;