'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.updateDatabase = undefined;

var _console = require('../../console');

var _console2 = _interopRequireDefault(_console);

var _createTask = require('../createTask');

var _createTask2 = _interopRequireDefault(_createTask);

var _firebaseTask = require('../firebaseTask');

var _firebaseTask2 = _interopRequireDefault(_firebaseTask);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var console = new _console2.default();
var firebaseT = new _firebaseTask2.default();

console.registerApp("SoapTask");
var actionUpdate = function actionUpdate() {
    // firebaseT.updateItems(()=>{
    //     firebaseT.updateInventario(()=>{
    //         firebaseT.updateClientes(()=>{
    //             console.red(firebaseT);

    //         });
    //     });
    // })
    firebaseT.updatePrecios(function (precios) {
        firebaseT.updateItems(precios, function (itemsData) {
            console.red("itemsData finish update");
        });
    });
    firebaseT.updateClientes(function () {
        console.red("Clientes update finish");
    });
};
firebaseT.listenOrders();
actionUpdate();

var updateDatabase = exports.updateDatabase = new _createTask2.default({
    time: '00 00 11,21 * * 0-6',
    actionUpdate: actionUpdate
});