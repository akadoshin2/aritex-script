'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _firebaseAdmin = require('firebase-admin');

var _firebaseAdmin2 = _interopRequireDefault(_firebaseAdmin);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _woocommerceApi = require('woocommerce-api');

var _woocommerceApi2 = _interopRequireDefault(_woocommerceApi);

var _nodeFetch = require('node-fetch');

var _nodeFetch2 = _interopRequireDefault(_nodeFetch);

var _awsSdk = require('aws-sdk');

var _awsSdk2 = _interopRequireDefault(_awsSdk);

var _console = require('../../console');

var _console2 = _interopRequireDefault(_console);

var _soap = require('../../soap');

var _soap2 = _interopRequireDefault(_soap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var serviceAccount = require("./serviceAccountKey.json");

_awsSdk2.default.config.update({ accessKeyId: 'AKIAIACSUYHJ4Q6WTHWQ', secretAccessKey: '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' });

var console = new _console2.default();

var WooCommerce = new _woocommerceApi2.default({
    url: 'https://aritex.com.co',
    consumerKey: 'ck_66655dac6a85f4ebb02af6ab993b524f43cd3bc2',
    consumerSecret: 'cs_20354a46d591550044b87fd0a5aa21ee6256ccf9',
    wpAPI: true,
    wpAPIPrefix: "",
    version: 'wc/v1'
});

Array.prototype.unique = function (a) {
    return function () {
        return this.filter(a);
    };
}(function (a, b, c) {
    return c.indexOf(a, b + 1) < 0;
});

console.registerApp("FirebaseTask", 'Register class FirebaseTask');

var FirebaseTask = function (_SoapRequest) {
    _inherits(FirebaseTask, _SoapRequest);

    function FirebaseTask(props) {
        _classCallCheck(this, FirebaseTask);

        var _this = _possibleConstructorReturn(this, (FirebaseTask.__proto__ || Object.getPrototypeOf(FirebaseTask)).call(this, props));

        _this.firebaseData = {
            credential: _firebaseAdmin2.default.credential.cert(serviceAccount),
            databaseURL: "https://reacttestaritex.firebaseio.com"
        };
        _firebaseAdmin2.default.initializeApp(_this.firebaseData);
        var db = _firebaseAdmin2.default.database();
        _this.dbRef = db.ref();
        return _this;
    }

    _createClass(FirebaseTask, [{
        key: 'uploadJsonFirebaseRest',
        value: function uploadJsonFirebaseRest(path, data) {
            var _this2 = this;

            return new Promise(function (resolve, reject) {
                var options = {
                    url: _this2.firebaseData.databaseURL + '/ThemeApp/data/' + path + '.json',
                    method: 'PUT',
                    body: JSON.stringify(data, null, 2)
                };
                console.yellow('Upload data to firebase in path /ThemeApp/data/' + path + '...');
                (0, _request2.default)(options, function (error, response, body) {
                    if (error) throw new Error(error);
                    console.cyan('Upload complete to firebase, path /ThemeApp/data/' + path + '.');
                    resolve();
                });
            });
        }
    }, {
        key: 'getImgs',
        value: async function getImgs(callback) {
            var s3bucket = new _awsSdk2.default.S3();
            var params = {
                Bucket: 'aritex-test',
                Prefix: 'products',
                Delimiter: 'products'
            };

            var imgs = null;
            await s3bucket.listObjects(params, function (err, data) {
                if (err) {
                    console.log(err);
                } else {
                    _fs2.default.writeFileSync('data/imgs.json', JSON.stringify(_defineProperty({}, "name", data), null, 2));
                    callback(data.Contents);
                    // $scope.allImageData = data.Contents;
                }
            });
        }
    }, {
        key: 'updateClientes',
        value: function updateClientes() {
            var _this3 = this;

            var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

            console.magenta("UpdateClientes in progress...");
            console.yellow('Updating clientes list...');
            this.getClientes(function (body, res, name) {
                var clientsList = [];
                var clientIdSucuId = [];
                body.forEach(function (cliente) {
                    // console.log(cliente);
                    //Model cliente
                    var addClient = true;
                    clientIdSucuId.forEach(function (idS) {
                        if (idS == '' + cliente.identificacion[0].trim()) {
                            addClient = false;
                            clientsList.forEach(function (client) {
                                if (idS == client.cc) {
                                    var addSucursal = true;
                                    client.id_sucursal.forEach(function (sucursal) {
                                        if (sucursal == cliente.id_sucursal[0].trim()) {
                                            addSucursal = false;
                                            return 0;
                                        }
                                    });
                                    if (addSucursal) {
                                        client.id_sucursal.push(cliente.id_sucursal[0].trim());
                                        if (cliente.nombre_sucursal) {
                                            client.nombre_sucursal.push(cliente.nombre_sucursal[0].trim());
                                        } else {
                                            client.nombre_sucursal.push('');
                                        }
                                        if (cliente.ciudad) {
                                            client.address_sucursal.push(cliente.ciudad[0].trim());
                                        } else {
                                            client.address_sucursal.push('');
                                        }
                                        if (cliente.address_sucursal) {
                                            client.city_sucursal.push(cliente.address_sucursal[0].trim());
                                        } else {
                                            client.city_sucursal.push('');
                                        }
                                    }
                                }
                            });
                        }
                    });
                    if (addClient) {
                        clientIdSucuId.push('' + cliente.identificacion[0].trim());
                        // console.log(cliente);
                        clientsList.push({
                            client_id: cliente.identificacion[0].trim(),
                            rowOrder: cliente['$']['msdata:rowOrder'].trim(),
                            cc: cliente.identificacion[0].trim(),
                            name: cliente.razon_social[0].trim(),
                            nombres: cliente.nombres[0].trim(),
                            apellido1: cliente.apellido1[0].trim(),
                            apellido2: cliente.apellido2[0].trim(),
                            id_sucursal: new Array(cliente.id_sucursal[0].trim()),
                            nombre_sucursal: new Array(cliente.nombre_sucursal ? cliente.nombre_sucursal[0].trim() : ''),
                            address_sucursal: new Array(cliente.direccion ? cliente.direccion[0].trim() : ''),
                            city_sucursal: new Array(cliente.ciudad ? cliente.ciudad[0].trim() : ''),

                            email: "email",
                            user_type: "user_type",
                            dv: "dv",
                            act_code: "act_code",
                            seller_code: "seller_code",
                            status: "status",
                            client_status: "client_status",
                            attachments: [],
                            time: "time",
                            flete: "flete",
                            assigned: "assigned",
                            treasure_dept: "treasure_dept",
                            mobile: "mobile",
                            phone: "phone",
                            city: "city",
                            address: "address"
                        });
                    }
                });
                var data = JSON.stringify(_defineProperty({}, name, clientsList), null, 2);
                _fs2.default.writeFileSync('data/clientsList.json', data);
                console.cyan(name + ' list update, width ' + clientsList.length + ' items.');
                if (clientsList.length > 0) {
                    _this3.uploadJsonFirebaseRest(name, clientsList).then(function () {
                        if (callback != null) callback(clientsList);
                    });
                }
            });
        }
    }, {
        key: 'updateInventario',
        value: function updateInventario() {
            var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

            console.magenta("UpdateInventario in progress...");
            console.yellow('Updating inventario list...');
            this.getInventario(function (body, res, name) {
                var inventarioList = {};
                body.forEach(function (indentario) {
                    //Model inventario
                    if (inventarioList[indentario.id_item[0].trim()] == undefined) {
                        inventarioList[indentario.id_item[0].trim()] = [];
                    }
                    inventarioList[indentario.id_item[0].trim()].push({
                        id: indentario['$']['diffgr:id'].trim(),
                        rowOrder: indentario['$']['msdata:rowOrder'].trim(),
                        co: indentario.co[0].trim(),
                        bodega: indentario.bodega[0].trim(),
                        id_item: indentario.id_item[0].trim(),
                        referencia: indentario.referencia[0].trim(),
                        descripcion: indentario.descripcion[0].trim(),
                        cantidad: indentario.CANTIDAD[0].trim(),
                        cant_comprometida: indentario.CANT_COMPROMETIDA[0].trim(),
                        cant_disponible: indentario.CANT_DISPONIBLE[0].trim()
                    });
                });
                var data = JSON.stringify(_defineProperty({}, name, inventarioList), null, 2);
                _fs2.default.writeFileSync('data/inventarioList.json', data);
                console.cyan(name + ' list update, width ' + inventarioList.length + ' items.');
                if (callback != null) callback(data);
                // if(inventarioList.length>0){
                // this.uploadJsonFirebaseRest(name, inventarioList).then(()=>{
                //     if(callback!=null) callback(data);
                // });
                // }
            });
        }
    }, {
        key: 'searchWord',
        value: function searchWord(inW, word) {
            var e = void 0;
            for (var a in inW.items) {
                inW.items[a].forEach(function (element) {
                    if (element.referencia_item == word) {
                        e = element.id_item;
                        return element.id_item;
                    }
                });
            }
            console.log(e);
            return e;
        }
    }, {
        key: 'listenOrders',
        value: function listenOrders() {
            var _this4 = this;

            var db = _firebaseAdmin2.default.database();
            var orderRef = db.ref("ThemeApp/data/orders");
            orderRef.on("value", function (snapshot) {
                var val = snapshot.val();

                var _loop = function _loop(valKey) {
                    var keyUnitPedido = valKey;
                    var valOrder = val[valKey];
                    if (valOrder.status == 2) {
                        console.log(keyUnitPedido);
                        var date = new Date();
                        var dateEntrega = new Date(valOrder.date_entrega);
                        var date_ = '' + date.getFullYear() + ("0" + (date.getMonth() + 1)).substr(-2) + ("0" + date.getDate()).substr(-2);
                        var date_entrega = '' + dateEntrega.getFullYear() + ("0" + (dateEntrega.getMonth() + 1)).substr(-2) + ("0" + dateEntrega.getDate()).substr(-2);
                        // eslint-disable-next-line radix
                        var uiid = parseInt(new Date().getTime() / 100);
                        var bodyXml = '';
                        var archivosP = _fs2.default.readFile('data/itemsList1.json', 'utf8', function (err, content) {
                            bodyXml = ('\n                            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gen="http://generictransfer.com/">\n                            <soapenv:Header/>\n                            <soapenv:Body>\n                            <gen:ImportarDatosXML>\n                                <gen:idDocumento>55766</gen:idDocumento>\n                                <!--Optional:-->\n                                <gen:strNombreDocumento>Pedido</gen:strNombreDocumento>\n                                <gen:idCompania>2</gen:idCompania>\n                                <!--Optional:-->\n                                <gen:strCompania>1</gen:strCompania>\n                                <!--Optional:-->\n                                <gen:strUsuario>gt</gen:strUsuario>\n                                <!--Optional:-->\n                                <gen:strClave>gt</gen:strClave>\n                                <!--Optional:-->\n                                <gen:strFuenteDatos><![CDATA[\n                                <MyDataset>\n                                <Pedidos>\n                                    <CentroOperacioDoctoc>001</CentroOperacioDoctoc>\n                                    <NumDocto>1</NumDocto>\n                                    <FechaDocto>' + date_ + '</FechaDocto>\n                                    <IndBackordeDocto>' + (valOrder.indBackordeDocto || 2) + '</IndBackordeDocto>\n                                    <TercerFact>' + valOrder.client_id + '</TercerFact>\n                                    <SucursalFact>' + valOrder.client.id_sucursal + '</SucursalFact>\n                                    <TerceroDesp>' + valOrder.client_id + '</TerceroDesp>\n                                    <SucursalDesp>' + valOrder.client.id_sucursal + '</SucursalDesp>\n                                    <CentroOperFac>001</CentroOperFac>\n                                    <FechaEntrega>' + valOrder.date_entrega + '</FechaEntrega>\n                                    <NumDiasEntrega>' + valOrder.num_entrega + '</NumDiasEntrega>\n                                    <OrdenCompra>OC' + uiid + '</OrdenCompra>\n                                    <CondPago>60D</CondPago>\n                                    <Observaciones>' + valOrder.observation + '</Observaciones>\n                                    <TerceroVend>' + valOrder.user + '</TerceroVend>\n                                </Pedidos>\n                                ' + valOrder.products.map(function (product) {
                                return product.variations.map(function (variation) {
                                    return '\n                                    <MovtoPedidos>\n                                        <UnidNegocMvto>99</UnidNegocMvto>\n                                        <CentroOper>001</CentroOper>\n                                        <ConsecDocto>1</ConsecDocto>\n                                        <NumReg>1</NumReg>\n                                        <Item>' + _this4.searchWord(JSON.parse(content), product.product.reference_id + '-' + variation.color + '-' + variation.size) + '</Item>\n                                        <Bodega>00101</Bodega>\n                                        <Motivo>01</Motivo>\n                                        <CentroOperMvto>001</CentroOperMvto>\n                                        <FechaEntrga>' + valOrder.date_entrega + '</FechaEntrga>\n                                        <NumDiasEntrega>' + valOrder.num_entrega + '</NumDiasEntrega>\n                                        <Cantidad>' + variation.quantity + '</Cantidad>\n                                        <IndBackorder>5</IndBackorder>\n                                    </MovtoPedidos>';
                                });
                            }) + '\n                        </MyDataset>]]>\n                                </gen:strFuenteDatos>\n                                <!--Optional:-->\n                                <gen:Path>C:\\inetpub\\wwwroot\\GTIntegration\\Planos</gen:Path>\n                            </gen:ImportarDatosXML>\n                            </soapenv:Body>\n                        </soapenv:Envelope>    \n                        ').replace(/,/g, '');
                            (0, _nodeFetch2.default)('http://190.85.249.115/GTIntegration/ServiciosWeb/wsGenerarPlano.asmx', {
                                method: 'POST',
                                headers: {
                                    "content-type": "text/xml",
                                    "soapaction": "http://generictransfer.com/ImportarDatosXML",
                                    "host": "190.85.249.115",
                                    "cache-control": "no-cache"
                                },
                                body: bodyXml
                            }).then(function (response) {
                                return response.text();
                            }).then(function (res) {
                                console.log(res);
                                if (res.search('<ImportarDatosXMLResult>Importacion exitosa</ImportarDatosXMLResult>')) {
                                    orderRef.child(keyUnitPedido).child('status').set('1');
                                } else {
                                    orderRef.child(keyUnitPedido).child('status').set('3');
                                }
                                _fs2.default.writeFileSync('data/resultFetch.json', res);
                                _fs2.default.writeFileSync('data/resulXml.json', bodyXml);
                                _fs2.default.writeFileSync('data/resultVal.json', JSON.stringify(valOrder, null, 2));
                            });
                        });
                    }
                };

                for (var valKey in val) {
                    _loop(valKey);
                }
            }, function (error) {
                console.log("The read Failed Orders: " + error.code);
            });
        }
    }, {
        key: 'filterCategory',
        value: function filterCategory(dummyData, imagenes, callback) {
            var categories = {};
            var i = 0;

            var db = _firebaseAdmin2.default.database();
            var orderRef = db.ref("ThemeApp/data/products");
            return orderRef.once("value", function (snapshot) {
                var productsFirebase = snapshot.val();
                console.log(productsFirebase);

                for (var a in dummyData) {
                    dummyData[a].forEach(function (element) {
                        if (categories[element.CATEGORIA] == undefined) {
                            if (element.CATEGORIA != undefined) {
                                var show = true,
                                    home = false;
                                if (productsFirebase !== null) {
                                    var _prod_index = productsFirebase.findIndex(function (_prod_) {
                                        return _prod_.id === element.CATEGORIA;
                                    });
                                    if (_prod_index !== -1) {
                                        home = productsFirebase[_prod_index].home;
                                        show = productsFirebase[_prod_index].shown;
                                    }
                                }
                                categories[element.CATEGORIA] = {
                                    "id": element.CATEGORIA,
                                    "updatedDate": new Date(),
                                    "createdDate": new Date(),
                                    "name": element.DEPARTAMENTO + ' ' + (element.DEPARTAMENTO.indexOf(element.CATEGORIA) >= 0 ? '' : element.CATEGORIA),
                                    "shown": show,
                                    "home": home,
                                    "order_id": i + 2,
                                    "status": 1,
                                    "slug": element.DEPARTAMENTO.toLowerCase() + ' ' + element.CATEGORIA.toLowerCase(),
                                    "display": "default",
                                    "menu_order": 0,
                                    "count": 0,
                                    "products": {}
                                };
                            }
                        }
                    });
                    i++;
                }

                var db = _firebaseAdmin2.default.database();
                var orderRef = db.ref("ThemeApp/data/colores");
                orderRef.once("value", function (snapshot) {
                    var colorsFirebase = snapshot.val();

                    for (var _a in dummyData) {
                        dummyData[_a].forEach(function (element) {
                            if (categories[element.CATEGORIA] != undefined) {
                                if (categories[element.CATEGORIA].products[element.REFERENCIA] == null) {
                                    categories[element.CATEGORIA].products[element.REFERENCIA] = {
                                        "updatedDate": new Date(),
                                        "createdDate": new Date(),
                                        "reference_id": element.REFERENCIA,
                                        "price": element.PRICE,
                                        "name": element.DESCRIPTION,
                                        "categories": [element.CATEGORIA],
                                        "total_sales": 0,
                                        "purchasable": true,
                                        "on_sale": false,
                                        "sale_price": "",
                                        "regular_price": element.PRICE,
                                        "description": '' + element.TELA,
                                        "catalog_visibility": "visible",
                                        "status": "publish",
                                        "type": "variable",
                                        "reference": element.REFERENCIA,
                                        "images": [],
                                        "attributes": [],
                                        "variations": {
                                            "color": [],
                                            "size": [],
                                            "colorId": [],
                                            "colorHex": []
                                        },
                                        "rating_count": 0,
                                        "default_attributes": []
                                    };
                                    imagenes.forEach(function (img) {
                                        var imgValid = img.Key.replace('products/', '');
                                        if (imgValid.split("-", 1)[0] == element.REFERENCIA) {
                                            // console.log(imgValid.split("-",  1)[0] == element.REFERENCIA)
                                            // console.log(`https://d1az3zemavhhvi.cloudfront.net/${(img.Key).replace('products/', '')}`)
                                            categories[element.CATEGORIA].products[element.REFERENCIA].images.push({
                                                "_id": element.ID,
                                                "updatedDate": new Date(),
                                                "createdDate": new Date(),
                                                "product_id": img.Key,
                                                "src": 'https://d1az3zemavhhvi.cloudfront.net/' + img.Key.replace('products/', '')
                                            });
                                        }
                                    });
                                    if (categories[element.CATEGORIA].products[element.REFERENCIA].images.length) categories[element.CATEGORIA].products[element.REFERENCIA].images.reverse();
                                }
                                if (categories[element.CATEGORIA].products[element.REFERENCIA].variations.color.indexOf(element.COLOR) < 0) {
                                    var _color_index = colorsFirebase.findIndex(function (_color_) {
                                        return element.COLOR.replace(' ', '').toLowerCase() === _color_.color.replace(' ', '').toLowerCase();
                                    });
                                    if (_color_index === -1) {
                                        console.log(element.COLOR);
                                    }
                                    categories[element.CATEGORIA].products[element.REFERENCIA].variations.color.push(element.COLOR);
                                    categories[element.CATEGORIA].products[element.REFERENCIA].variations.colorId.push(_color_index !== -1 ? colorsFirebase[_color_index].colorId : '000');
                                    categories[element.CATEGORIA].products[element.REFERENCIA].variations.colorHex.push(_color_index !== -1 ? colorsFirebase[_color_index].hex : '#FFF');
                                }
                                if (categories[element.CATEGORIA].products[element.REFERENCIA].variations.size.indexOf(element.TALLA) < 0) categories[element.CATEGORIA].products[element.REFERENCIA].variations.size.push(element.TALLA);

                                var sizes = categories[element.CATEGORIA].products[element.REFERENCIA].variations.size;
                                categories[element.CATEGORIA].products[element.REFERENCIA].variations.size.sort(function (a, b) {
                                    return ('' + a).localeCompare(b);
                                });
                                categories[element.CATEGORIA].products[element.REFERENCIA].variations.size.reverse();
                                if (!(parseInt(sizes[0]) >= 0)) {
                                    var sizes2 = [];
                                    sizes.forEach(function (size) {
                                        if (size === 'S' || size === 's') sizes2[0] = size;else if (size === 'M' || size === 'm') sizes2[1] = size;else if (size === 'L' || size === 'l') sizes2[2] = size;else if (size === 'XL' || size === 'xl') sizes2[3] = size;else if (size === 'XXL' || size === 'xxl') sizes2[4] = size;
                                    });
                                    categories[element.CATEGORIA].products[element.REFERENCIA].variations.size = sizes2.filter(function (s) {
                                        return 1;
                                    });
                                } else {
                                    var _sizes = [];
                                    sizes.forEach(function (size) {
                                        if (size == '4') _sizes[0] = size;else if (size == '8') _sizes[1] = size;else if (size == '12') _sizes[2] = size;else if (size == '16') _sizes[3] = size;else if (size == '18') _sizes[4] = size;
                                    });
                                    categories[element.CATEGORIA].products[element.REFERENCIA].variations.size = _sizes.filter(function (s) {
                                        return 1;
                                    });
                                }
                            }
                        });
                        dummyData[_a].forEach(function (element) {
                            if (categories[element.CATEGORIA].products[element.REFERENCIA].images.length <= 0) {
                                categories[element.CATEGORIA].products[element.REFERENCIA].images.push({
                                    "_id": "0001",
                                    "updatedDate": new Date(),
                                    "createdDate": new Date(),
                                    "product_id": "",
                                    "src": 'https://d1az3zemavhhvi.cloudfront.net/'
                                });
                            }
                            if (_typeof(categories[element.CATEGORIA]) === 'object') {
                                categories[element.CATEGORIA].count = categories[element.CATEGORIA].products.length;
                            }
                        });
                    }
                    console.log("termino", categories);
                    callback(categories);
                });
            });
        }
    }, {
        key: 'finishFilter',
        value: function finishFilter(data) {
            var products = [];
            for (var p in data) {
                var _product = [];
                for (var _p in data[p].products) {
                    _product.push(data[p].products[_p]);
                }
                data[p].products = _product;
                _product = [];
                products.push(data[p]);
            }
            return products;
        }
    }, {
        key: 'updateItems',
        value: function updateItems(prices) {
            var _this5 = this;

            var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

            console.magenta("UpdateItems in progress...");
            console.yellow('Updating items list...');
            this.getItems(function (body, res, name) {
                var itemsList = {};
                _fs2.default.writeFileSync('data/itemsListbody.json', JSON.stringify(_defineProperty({}, name, body), null, 2));
                body.forEach(function (items) {
                    //Model Items
                    if (itemsList[items.id_item[0].trim()] == undefined) {
                        itemsList[items.id_item[0].trim()] = [];
                    }
                    itemsList[items.id_item[0].trim()].push({
                        id: items['$']['diffgr:id'].trim(),
                        rowOrder: items['$']['msdata:rowOrder'].trim(),
                        id_item: items.id_item[0].trim(),
                        referencia_item: items.referencia_item[0].trim(),
                        descripcion: items.descripcion[0].trim(),
                        // barras: items.barras[0].trim(),
                        planes: items.planes[0].trim(),
                        criterio: items.criterio[0].trim(),
                        descripcion_criterio: items.descripcion_criterio[0].trim(),
                        descripcion_plan: items.descripcion_plan[0].trim()
                    });
                });

                var table = {};

                var _loop2 = function _loop2(idElement) {
                    var column = {};
                    itemsList[idElement].forEach(function (element) {
                        if (column.ID == undefined) {
                            column.ID = element.id_item;
                            column.DESCRIPTION = element.descripcion;
                            column.REFERENCIA_REQUEST = element.referencia_item.replace(' ', '_');
                            column.BARRAS = element.barras;
                        }
                        column[element.descripcion_plan] = element.descripcion_criterio;
                        if (_typeof(prices[element.referencia_item.replace(' ', '_')]) === 'object') {
                            column.PRICE = parseInt(prices[element.referencia_item.replace(' ', '_')].precio.replace('.0000', ''));
                        }
                    });
                    if (column != null) {
                        if (table['' + column.REFERENCIA] == undefined) {
                            table['' + column.REFERENCIA] = [];
                        }
                        table['' + column.REFERENCIA].push(column);
                    }
                };

                for (var idElement in itemsList) {
                    _loop2(idElement);
                }

                _fs2.default.writeFileSync('data/itemsList1.json', JSON.stringify(_defineProperty({}, name, itemsList), null, 2));

                _this5.getImgs(function (imgs) {
                    _this5.filterCategory(table, imgs, function (PROMISE_cat) {
                        var cat_ = PROMISE_cat;
                        _fs2.default.writeFileSync('data/itemsList1_1.json', JSON.stringify(_defineProperty({}, name, table), null, 2));
                        _fs2.default.writeFileSync('data/itemsList2.json', JSON.stringify(_defineProperty({}, name, cat_), null, 2));
                        var table2 = _this5.finishFilter(cat_);
                        var data = JSON.stringify(_defineProperty({}, name, table2), null, 2);

                        setTimeout(function () {
                            table2.forEach(function (itemFor) {
                                itemFor.products.forEach(function (productsFor) {
                                    var dataSend = {
                                        name: productsFor.name,
                                        regular_price: '' + productsFor.regular_price,
                                        sale_price: '' + productsFor.price,
                                        type: 'simple',
                                        status: "private",
                                        description: productsFor.description,
                                        short_description: productsFor.description,
                                        catalog_visibility: productsFor.catalog_visibility,
                                        sku: '' + productsFor.reference,
                                        images: [],
                                        attributes: [{
                                            name: 'Talla',
                                            position: 0,
                                            visible: true,
                                            variation: true,
                                            options: []
                                        }, {
                                            name: 'Color',
                                            position: 0,
                                            visible: true,
                                            variation: true,
                                            options: []
                                        }]
                                    };
                                    productsFor.images.forEach(function (imageProduct) {
                                        dataSend.images.push({
                                            src: imageProduct.src
                                        });
                                    });
                                    productsFor.variations.color.forEach(function (colorProduct) {
                                        dataSend.attributes[0].options.push(colorProduct);
                                    });
                                    productsFor.variations.size.forEach(function (sizeProduct) {
                                        dataSend.attributes[1].options.push(sizeProduct);
                                    });
                                    var sendRequest = new Promise(function (resolver, reject) {
                                        WooCommerce.post('products', dataSend, function (err, data, res) {
                                            if (err) {
                                                reject();
                                            }
                                            resolver(data, res);
                                        });
                                    });

                                    sendRequest.then(function (data, res) {
                                        console.log(res);
                                    });
                                });
                            });
                        }, 30000);

                        _fs2.default.writeFileSync('data/itemsList3.json', data);
                        console.cyan(name + ' list update, width ' + data.length + ' items.');
                        if (data.length > 0) {
                            _this5.uploadJsonFirebaseRest("products", table2).then(function () {
                                if (callback != null) callback(table2);
                            });
                        }
                    });
                });
            });
        }
    }, {
        key: 'joinPrices',
        value: function joinPrices(prices) {
            var finishPrice = {};
            prices.forEach(function (price) {
                finishPrice[price.referencia.replace(' ', '_')] = {
                    precio: price.precio,
                    fecha: price.fecha_act
                };
            });
            return finishPrice;
        }
    }, {
        key: 'updatePrecios',
        value: function updatePrecios() {
            var _this6 = this;

            var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

            console.yellow('Updating precios list...');
            this.getPrecios(function (body, res, name) {
                var preciosList = [];
                _fs2.default.writeFileSync('data/preciosListbody.json', JSON.stringify(_defineProperty({}, name, body), null, 2));
                body.forEach(function (precio) {
                    //Model Precios
                    preciosList.push({
                        id: precio['$']['diffgr:id'].trim(),
                        rowOrder: precio['$']['msdata:rowOrder'].trim(),
                        id_lista: precio.id_lista[0].trim(),
                        id_item: precio.id_item[0].trim(),
                        referencia: precio.referencia[0].trim(),
                        descripcion: precio.descripcion[0].trim(),
                        precio: precio.precio[0].trim(),
                        fecha_act: precio.fecha_act[0].trim(),
                        fecha_inact: precio.fecha_inact[0].trim()
                    });
                });
                var joinP = _this6.joinPrices(preciosList);
                var data = JSON.stringify(_defineProperty({}, name, preciosList), null, 2);
                _fs2.default.writeFileSync('data/preciosList.json', data);
                _fs2.default.writeFileSync('data/preciosList1.json', JSON.stringify(_defineProperty({}, name, joinP), null, 2));
                console.cyan(name + ' list update, width ' + preciosList.length + ' items.');
                if (preciosList.length > 0) {
                    if (callback != null) callback(joinP);
                    // this.uploadJsonFirebaseRest(name, preciosList).then(()=>{
                    //     if(callback!=null) callback();
                    // });
                }
            });
        }
    }, {
        key: 'allRequestDone',
        value: function allRequestDone() {
            return this.allDone(true);
        }
    }]);

    return FirebaseTask;
}(_soap2.default);

exports.default = FirebaseTask;