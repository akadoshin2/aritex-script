'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _cron = require('cron');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// Seconds: 0-59
// Minutes: 0-59
// Hours: 0-23
// Day of Month: 1-31
// Months: 0-11 (Jan-Dec)
// Day of Week: 0-6 (Sun-Sat)

var CreateTast = function () {
    function CreateTast(props) {
        _classCallCheck(this, CreateTast);

        var time = props.time ? props.time : '* * * * * *';
        this.action = props.action ? props.action : null;
        this.task = new _cron.CronJob({
            cronTime: time,
            onTick: this.action,
            start: false,
            timeZone: 'America/Los_Angeles'
        });
    }

    _createClass(CreateTast, [{
        key: 'registerTime',
        value: function registerTime(time) {
            this.task.setTime((0, _cron.CronTime)(time));
        }
    }, {
        key: 'run',
        value: function run() {
            this.task.start();
        }
    }, {
        key: 'isRunning',
        value: function isRunning() {
            return this.task.running;
        }
    }, {
        key: 'stop',
        value: function stop() {
            this.task.stop();
        }
    }]);

    return CreateTast;
}();

exports.default = CreateTast;