"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Consol = function () {
    function Consol() {
        _classCallCheck(this, Consol);

        this.appRegister = "";
        this.colors = {
            Black: "\x1b[30m",
            Red: "\x1b[31m",
            Green: "\x1b[32m",
            Yellow: "\x1b[33m",
            Blue: "\x1b[34m",
            Magenta: "\x1b[35m",
            Cyan: "\x1b[36m",
            White: "\x1b[37m"
        };
        this.backgrounds = {
            Black: "\x1b[40m",
            Red: "\x1b[41m",
            Green: "\x1b[42m",
            Yellow: "\x1b[43m",
            Blue: "\x1b[44m",
            Magenta: "\x1b[45m",
            Cyan: "\x1b[46m",
            White: "\x1b[47m"
        };
        this.methods = {
            Reset: "\x1b[0m",
            Bright: "\x1b[1m",
            Dim: "\x1b[2m",
            Underscore: "\x1b[4m",
            Blink: "\x1b[5m",
            Reverse: "\x1b[7m",
            Hidden: "\x1b[8m"
        };
    }

    _createClass(Consol, [{
        key: "print",
        value: function print(text, color, background) {
            var method = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "Reset";

            if ((typeof text === "undefined" ? "undefined" : _typeof(text)) == 'object') {
                console.log(text, { depth: null });
                return 0;
            }
            console.log("" + this.colors[color] + this.backgrounds[background] + "%s" + this.methods[method], "" + this.appRegister + text);
        }
    }, {
        key: "log",
        value: function log(text) {
            console.log(text);
        }
    }, {
        key: "red",
        value: function red(text) {
            var background = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'Black';

            this.print(text, "Red", background);
        }
    }, {
        key: "yellow",
        value: function yellow(text) {
            var background = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'Black';

            this.print(text, "Yellow", background);
        }
    }, {
        key: "green",
        value: function green(text) {
            var background = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'Black';

            this.print(text, "Green", background);
        }
    }, {
        key: "cyan",
        value: function cyan(text) {
            var background = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'Black';

            this.print(text, "Cyan", background);
        }
    }, {
        key: "magenta",
        value: function magenta(text) {
            var background = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'Black';

            this.print(text, "Magenta", background);
        }
    }, {
        key: "registerApp",
        value: function registerApp(name) {
            var title = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : name;

            this.green(" - " + title);
            this.appRegister = "[" + name + "] ";
        }
    }]);

    return Consol;
}();

exports.default = Consol;