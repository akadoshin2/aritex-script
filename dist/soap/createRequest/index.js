'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _console = require('../../console');

var _console2 = _interopRequireDefault(_console);

var _xml2js = require('xml2js');

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var console = new _console2.default();

console.registerApp("CreateRequest", 'Register class CreateRequest');

var createRequest = function () {
    function createRequest(props) {
        _classCallCheck(this, createRequest);

        this.GetDataUrl = 'http://190.85.249.115/WSUNOEE/WSUNOEE.asmx';
        this.headers = {
            'Content-Type': 'text/xml',
            'cache-control': 'no-cache'
        };
        this.options = {
            url: this.GetDataUrl,
            method: 'POST',
            qs: { WSDL: '' },
            headers: this.headers,
            body: ""
        };
    }

    _createClass(createRequest, [{
        key: 'on',
        value: function on(body, callback) {
            console.yellow('Request loading...');
            this.options.body = '\n        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">\n        <soapenv:Header/>\n        <soapenv:Body>\n            <tem:EjecutarConsultaXML>\n                <!--Optional:-->\n                <tem:pvstrxmlParametros>\n                <![CDATA[\n                    <Consulta>\n                        <NombreConexion>real</NombreConexion>\n                        <IdCia>1</IdCia>\n                        <IdProveedor>IS</IdProveedor>\n                        <IdConsulta>' + body + '</IdConsulta>\n                        <Usuario>usuariogt</Usuario>\n                        <Clave>gt2017</Clave>\n                        <Parametros></Parametros>\n                    </Consulta> \n                ]]>\n                </tem:pvstrxmlParametros>\n            </tem:EjecutarConsultaXML>\n        </soapenv:Body>\n        </soapenv:Envelope>';
            //console.cyan(this.options);
            (0, _request2.default)(this.options, function (error, response, body) {
                if (error) {
                    console.log(error, response, body);
                    throw new Error(error);
                }
                (0, _xml2js.parseString)(body, function (err, result) {
                    try {
                        var _result = result['soap:Envelope']['soap:Body'][0]['EjecutarConsultaXMLResponse'][0]['EjecutarConsultaXMLResult'][0]['diffgr:diffgram'][0]['NewDataSet'][0]['Resultado'];
                        callback(error, response, _result);
                    } catch (error) {
                        var _error = result['soap:Envelope']['soap:Body'][0]['soap:Fault'][0]['faultstring'][0];
                        console.red(_error);
                    }
                });
                console.cyan("Request finish");
            });
        }
    }]);

    return createRequest;
}();

exports.default = createRequest;