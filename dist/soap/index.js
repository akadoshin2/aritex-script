'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _createRequest = require('./createRequest');

var _createRequest2 = _interopRequireDefault(_createRequest);

var _console = require('../console');

var _console2 = _interopRequireDefault(_console);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var console = new _console2.default();

console.registerApp("SoapResquest", 'Register class SoapResquest');

var SoapResquest = function (_CreateRequest) {
    _inherits(SoapResquest, _CreateRequest);

    function SoapResquest(props) {
        _classCallCheck(this, SoapResquest);

        var _this = _possibleConstructorReturn(this, (SoapResquest.__proto__ || Object.getPrototypeOf(SoapResquest)).call(this, props));

        _this.Promises = {
            PromiseClientes: null,
            PromiseInventario: null,
            PromiseItems: null,
            PromisePrecios: null
        };
        return _this;
    }

    _createClass(SoapResquest, [{
        key: '_PromiseRequest',
        value: function _PromiseRequest(name) {
            var _this2 = this;

            console.yellow('Request from ' + name + '...');
            return new Promise(function (resolve, reject) {
                _this2.on(name, function (error, response, body) {
                    resolve({ error: error, response: response, body: body, name: name });
                });
            });
        }
    }, {
        key: 'getClientes',
        value: function getClientes(callback) {
            this.Promises.PromiseClientes = this._PromiseRequest('clientes').then(function (_ref) {
                var response = _ref.response,
                    body = _ref.body,
                    name = _ref.name;

                console.cyan('Request from ' + name + ' finish, width ' + body.length + ' items.');
                callback(body, response, name);
            });
        }
    }, {
        key: 'getInventario',
        value: function getInventario(callback) {
            this.Promises.PromiseInventario = this._PromiseRequest('inventario').then(function (_ref2) {
                var response = _ref2.response,
                    body = _ref2.body,
                    name = _ref2.name;

                console.cyan('Request from ' + name + ' finish, width ' + body.length + ' items.');
                callback(body, response, name);
            });
        }
    }, {
        key: 'getItems',
        value: function getItems(callback) {
            this.Promises.PromiseItems = this._PromiseRequest('items').then(function (_ref3) {
                var response = _ref3.response,
                    body = _ref3.body,
                    name = _ref3.name;

                console.cyan('Request from ' + name + ' finish, width ' + body.length + ' items.');
                callback(body, response, name);
            });
        }
    }, {
        key: 'getPrecios',
        value: function getPrecios(callback) {
            this.Promises.PromisePrecios = this._PromiseRequest('precios').then(function (_ref4) {
                var response = _ref4.response,
                    body = _ref4.body,
                    name = _ref4.name;

                console.cyan('Request from ' + name + ' finish, width ' + body.length + ' items.');
                callback(body, response, name);
            });
        }
    }, {
        key: 'allDone',
        value: function allDone() {
            var ignore = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

            var allP = [];
            for (var Ps in this.Promises) {
                allP.push(this.Promises[Ps]);
                if (!ignore && this.Promises[Ps] === null) {
                    console.red('The ' + Ps + ' promise is null');
                    return Promise.reject(new Error('The ' + Ps + ' promise is null'));
                }
            }
            return Promise.all(allP).then(function (values) {
                console.cyan('All the promises ended');
                return values;
            });
        }
    }]);

    return SoapResquest;
}(_createRequest2.default);

exports.default = SoapResquest;